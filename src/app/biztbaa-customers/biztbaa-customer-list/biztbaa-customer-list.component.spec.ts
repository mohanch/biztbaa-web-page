import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiztbaaCustomerListComponent } from './biztbaa-customer-list.component';

describe('BiztbaaCustomerListComponent', () => {
  let component: BiztbaaCustomerListComponent;
  let fixture: ComponentFixture<BiztbaaCustomerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiztbaaCustomerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiztbaaCustomerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
