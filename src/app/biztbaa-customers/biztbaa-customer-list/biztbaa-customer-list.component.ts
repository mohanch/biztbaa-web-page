import { Component, OnInit, ViewChild } from '@angular/core';
import { BiztbaaCustomerService } from 'src/app/service/biztbaa-customer.service';
import { ToastrService } from 'src/app/service/toastr.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute,Router } from '@angular/router';
import { ConfirmDialogService } from 'src/app/service/confirm-dialog.service';
import { TEXT_CONFIG } from 'src/resources/texts/BiztbaaCustomerEntry.text';

/*
 * BiztbaaCustomerListComponent is used as a component for List page to show the customer details.
 *
 * @author Mohan Chidambaram
 */
@Component({
  selector: 'app-biztbaa-customer-list',
  templateUrl: './biztbaa-customer-list.component.html',
  styleUrls: ['./biztbaa-customer-list.component.css']
})
export class BiztbaaCustomerListComponent implements OnInit {

  constructor(protected route:ActivatedRoute,
    protected router:Router,
    public service:BiztbaaCustomerService,
    public toastrService:ToastrService,
    public confirmDialogService:ConfirmDialogService) { }

  text: any = TEXT_CONFIG;
  @ViewChild(MatSort, {static: true}) sorting: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginators: MatPaginator;
  searchKey: string;
  customerListTableData:MatTableDataSource<any>;
  columnHeaderList:String[] = [
    'contact',
    'name',
    'phone',
    'mail',
    'place',
    'birthDate',
    'status',
    'actions'
  ];
  showSpinner:boolean = false;
  
  /*
   * ngOnInit is Init Life cycle hook method.
   *
   */
  ngOnInit(): void {
    this.showSpinner =true;
    this.reloadList();
    this.showSpinner=false;
  }

  /*
   * onSearchClear is used to load all the customer detials on clearing the search value.
   *
   */
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  /*
   * applyFilter is used to load the customers based on the search key.
   *
   */
  applyFilter() {
    this.customerListTableData.filter = this.searchKey.trim().toLowerCase();
  }

  /*
   * onAddCustomer is used to load Add page when Add button is clicked.
   *
   */
  onAddCustomer() {
    let url:any[] = ['/add'];
    this.router.navigate(url, { relativeTo:this.route});
  }

  /*
   * onViewClick is used to load View page when View icon is clicked.
   * @param {any} row. 
   */
  onViewClick(row) {
    let url:any[] = ['/view'];
    this.router.navigate(url, { relativeTo:this.route,
    queryParams:{row: JSON.stringify(row)}});
  }

  /*
   * onEditClick is used to load Edit page when Edit icon is clicked.
   * @param {any} row.
   */
  onEditClick(row) {
    let url:any[] = ['/edit'];
    this.router.navigate(url, { relativeTo:this.route,
      queryParams:{row: JSON.stringify(row)}});
  }

  /*
   * onDeleteClick is used to delete a customer when delete icon is clicked.
   * @param {any} row.
   */
  onDeleteClick(row) {
    this.confirmDialogService.openConfirmationdialog(this.text.deleteConfirmationContent)
    .afterClosed().subscribe(response =>{
      if (response) {
        this.showSpinner =true;
        this.service.deleteCustomer(row).subscribe(
          (data: any) => {
            this.toastrService.success(this.text.deletedSuccessToastrMessage);
            this.reloadList();
            this.showSpinner =false;
        });
      }
    });
  }

  /*
   * reloadList is used to reload the list of customer data.
   *
   */
  reloadList() {
    this.service.getCustomerList().subscribe(
      (dataList: Array<any>) => {
        dataList.forEach(customerData => {
          if(customerData.status === '1') {
            customerData.status = this.text.activeStatus;
          } else {
            customerData.status = this.text.inactiveStatus;
          }
        });
        let customerList:Array<any> = dataList;
        this.customerListTableData = new MatTableDataSource(customerList);
        this.customerListTableData.sort = this.sorting;
        this.customerListTableData.paginator = this.paginators;
      }
    );
  }
}
