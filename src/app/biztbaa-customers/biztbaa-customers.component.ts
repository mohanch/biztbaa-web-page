import { Component, OnInit } from '@angular/core';
import { TEXT_CONFIG } from "../../resources/texts/BiztbaaCustomerEntry.text";

/*
 * BiztbaaCustomersComponent is used as a main component.
 *
 * @author Mohan Chidambaram
 */
@Component({
  selector: 'app-biztbaa-customers',
  templateUrl: './biztbaa-customers.component.html',
  styleUrls: ['./biztbaa-customers.component.css']
})
export class BiztbaaCustomersComponent implements OnInit {

  constructor() { }

  text: any = TEXT_CONFIG;

  ngOnInit(): void {
  }

}
