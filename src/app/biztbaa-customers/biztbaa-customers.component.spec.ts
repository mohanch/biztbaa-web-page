import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiztbaaCustomersComponent } from './biztbaa-customers.component';

describe('BiztbaaCustomersComponent', () => {
  let component: BiztbaaCustomersComponent;
  let fixture: ComponentFixture<BiztbaaCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiztbaaCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiztbaaCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
