import { Component, OnInit } from '@angular/core';
import { BiztbaaCustomerService } from 'src/app/service/biztbaa-customer.service';
import { ToastrService } from "src/app/service/toastr.service";
import { ActivatedRoute,Router } from '@angular/router';
import { ConfirmDialogService } from 'src/app/service/confirm-dialog.service';
import {FormControl} from '@angular/forms';
import { TEXT_CONFIG } from "../../../resources/texts/BiztbaaCustomerEntry.text";

/*
 * BiztbaaCustomerAddComponent is used as a component for Add, View and Edit page.
 *
 * @author Mohan Chidambaram
 */
@Component({
  selector: 'app-biztbaa-customer-add',
  templateUrl: './biztbaa-customer-add.component.html',
  styleUrls: ['./biztbaa-customer-add.component.css']
})
export class BiztbaaCustomerAddComponent implements OnInit {

  constructor(protected route:ActivatedRoute,
    protected router:Router,
    public service:BiztbaaCustomerService,
    public toastrService:ToastrService,
    public confirmDialogService:ConfirmDialogService) { }

  text: any = TEXT_CONFIG;
  addButtonText:string = this.text.addButtonText;
  addHeaderTitle:string = this.text.addHeaderTitle;
  isView:boolean;
  isEdit:boolean;
  showSpinner:boolean = false;
  dateFormat = new FormControl(new Date());
  viewPath:string = 'view';
  editPath:string = 'edit';
  activeValue:string = '1';
  inactiveValue:string = '2';

  /*
   * ngOnInit is Init Life cycle hook method.
   *
   */
  ngOnInit(): void {  
    let rootPath = this.route.snapshot.routeConfig.path;
    this.isView = rootPath === this.viewPath;
    this.isEdit = rootPath === this.editPath;
    if (this.isView || this.isEdit) {
      let rowData = this.route.snapshot.queryParamMap.get('row');
      if (rowData) {
        let parsedRow = JSON.parse(rowData);
        parsedRow.status = parsedRow.status === this.text.activeStatus ? 
          this.activeValue : this.inactiveValue;
        parsedRow.birthDate = new Date(parsedRow.birthDate);
        this.service.form.setValue(parsedRow);
      }
      this.addButtonText = this.text.updateButtonText;
      this.addHeaderTitle = this.isView ? this.text.viewPageHeader : this.text.editPageHeader;
    }
  }

  /*
   * onClear is used to clear the data in the form.
   *
   */
  onClear() {
    this.service.form.reset();
    this.service.setDefault();
  }

  /*
   * onAddCustomer is used to add and update the customer details.
   *
   */
  onAddCustomer() {
    if (this.service.form.valid) {
      this.showSpinner =true;
      this.isEdit ? this.service.updateCustomerDetail(this.service.form.value).subscribe(
        (data: any) => {
          this.service.form.reset();
          this.service.setDefault();
          this.toastrService.success(this.isEdit ? this.text.updateSuccessToastrMessage : 
            this.text.addSuccessToastrMessage);
          this.showSpinner =false;
          this.onBackButtonClick();
        }
      ) :
      this.service.addCustomer(this.service.form.value).subscribe(
        (data: any) => {
          this.service.form.reset();
          this.service.setDefault();
          this.toastrService.success(this.isEdit ? this.text.updateSuccessToastrMessage : 
            this.text.addSuccessToastrMessage);
          this.showSpinner =false;
          this.onBackButtonClick();
        }
      );
    } else {
      this.toastrService.fail(this.text.addFailiureToastrMessage);
    }
  }

  /*
   * onBackButtonClick is used to load list page when back button is clicked.
   *
   */
  onBackButtonClick() {
    let url:any[] = ['/list'];
    this.router.navigate(url, { relativeTo:this.route});
    this.onClear();
  }

  /*
   * onEditClick is used to load edit page when edit icon is clicked.
   *
   */
  onEditClick() {
    let url:any[] = ['/edit'];
    this.router.navigate(url, { relativeTo:this.route});
  }

  /*
   * onDeleteClick is used to delete the customer when delete icon is clicked.
   *
   */
  onDeleteClick() {
    this.confirmDialogService.openConfirmationdialog(this.text.deleteConfirmationContent)
    .afterClosed().subscribe(response =>{
      if (response) {
        this.service.deleteCustomer(this.service.form.value).subscribe(
          (data: any) => {
            this.toastrService.success(this.text.deletedSuccessToastrMessage);
            let url:any[] = ['/list'];
            this.router.navigate(url, { relativeTo:this.route});
            this.onClear();
        });
      }
    });
  }

}
