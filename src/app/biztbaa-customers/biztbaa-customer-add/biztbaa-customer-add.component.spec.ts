import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiztbaaCustomerAddComponent } from './biztbaa-customer-add.component';

describe('BiztbaaCustomerAddComponent', () => {
  let component: BiztbaaCustomerAddComponent;
  let fixture: ComponentFixture<BiztbaaCustomerAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiztbaaCustomerAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiztbaaCustomerAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
