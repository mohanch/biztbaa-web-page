import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BiztbaaCustomersComponent } from './biztbaa-customers/biztbaa-customers.component';
import { BiztbaaCustomerAddComponent } from './biztbaa-customers/biztbaa-customer-add/biztbaa-customer-add.component';

const routes: Routes = [
  {path: "", pathMatch: "full", redirectTo: "/list"},
  {path: "list", component: BiztbaaCustomersComponent},
  {path: "add", component: BiztbaaCustomerAddComponent},
  {path: "view", component: BiztbaaCustomerAddComponent},
  {path: "edit", component: BiztbaaCustomerAddComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
