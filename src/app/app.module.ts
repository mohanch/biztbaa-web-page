import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BiztbaaCustomersComponent } from './biztbaa-customers/biztbaa-customers.component';
import { BiztbaaCustomerListComponent } from './biztbaa-customers/biztbaa-customer-list/biztbaa-customer-list.component';
import { CustomerModule } from "./customer/customer.module";
import { HttpClientModule } from "@angular/common/http";
import { BiztbaaCustomerService } from "./service/biztbaa-customer.service";
import { BiztbaaCustomerAddComponent } from './biztbaa-customers/biztbaa-customer-add/biztbaa-customer-add.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    BiztbaaCustomersComponent,
    BiztbaaCustomerListComponent,
    BiztbaaCustomerAddComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CustomerModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    BiztbaaCustomerService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmDialogComponent]
})
export class AppModule { }
