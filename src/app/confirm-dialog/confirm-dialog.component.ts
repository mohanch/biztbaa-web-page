import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

/*
 * ConfirmDialogComponent is used as a component for confirmation dialog.
 *
 * @author Mohan Chidambaram
 */
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data,
  public dialogRef: MatDialogRef<ConfirmDialogComponent>) { }

  /*
   * ngOnInit is Init Life cycle hook method.
   *
   */
  ngOnInit(): void {
  }

  /*
   * closeConfirmationDialog is used to close the confirmation dialog shown.
   *
   */
  closeConfirmationDialog() {
    this.dialogRef.close(false);
  }

}
