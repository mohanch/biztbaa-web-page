import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { TEXT_CONFIG } from 'src/resources/texts/BiztbaaCustomerEntry.text';

/*
 * BiztbaaCustomerService is used for API requests and also provides mock data to the screen.
 *
 * @author Mohan Chidambaram
 */
@Injectable({
  providedIn: 'root'
})
export class BiztbaaCustomerService {

  constructor(private http:HttpClient) { }

  readonly rootUrl = 'https://quiz-page.herokuapp.com';
  text: any = TEXT_CONFIG;
  form: FormGroup = new FormGroup({
    key: new FormControl(null),
    name: new FormControl('', Validators.required),
    phone: new FormControl('', [Validators.required, Validators.minLength(10)]),
    mail: new FormControl('', [Validators.required, Validators.email]),
    place: new FormControl(''),
    birthDate: new FormControl(''),
    status: new FormControl('2')
  });

  customerList:Array<any> = [
    {
      key: 1,
      name: 'Nazriya',
      phone: '7854129635',
      mail: 'naz@outlook.com',
      place: 'Kerala',
      birthDate: 'Thu Mar 11 1993',
      status: '1'
    },
    {
      key: 2,
      name: 'G V Prakash Kumar',
      phone: '9865327419',
      mail: 'gvp@yahoo.com',
      place: 'Chennai',
      birthDate: 'Wed Apr 05 1995',
      status: '1'
    },
    {
      key: 3,
      name: 'Dravid Rahul',
      phone: '7412589637',
      mail: 'rahul@redif.com',
      place: 'Mumbai',
      birthDate: 'Mon Oct 19 1992',
      status: '2'
    },
    {
      key: 4,
      name: 'Preethi',
      phone: '8796541230',
      mail: 'kpm@outlook.com',
      place: 'Namakkal',
      birthDate: 'Fri May 21 1999',
      status: '2'
    },
    {
      key: 5,
      name: 'Mohan',
      phone: '9994128922',
      mail: 'mohan@gmail.com',
      place: 'Erode',
      birthDate: 'Mon Dec 30 1996',
      status: '1'
    },
    {
      key: 6,
      name: 'Vimal',
      phone: '9870654123',
      mail: 'vimal@yahoo.com',
      place: 'Virudhunagar',
      birthDate: 'Fri Feb 28 1997',
      status: '2'
    }
  ];

  /*
   * setDefault is used to set the default value the form.
   *
   */
  setDefault() {
    this.form.setValue({
      key: null,
      name: '',
      phone: '',
      mail: '',
      place: '',
      birthDate: '',
      status: '2'
    })
  }

  /*
   * addCustomer is used to add the customer details.
   * @param {any} customer.
   */
  addCustomer(customer) {
     let newEmployee : { [key:string]: any} = {
      name: customer.name,
      phone: customer.phone,
      mail: customer.mail,
      place: customer.place,
      birthDate: customer.birthDate ? customer.birthDate.toDateString() : '',
      status: customer.status
    };
    // return this.http.post(this.rootUrl + '/addEmployee', newEmployee);
    return this.http.post('http://localhost:8080/addEmployee', newEmployee);
  }

  /*
   * getCustomerList is used to get the list of customer details.
   * 
   */
  getCustomerList() {
    // return this.http.post(this.rootUrl + '/getAllEmployee');
    return this.http.get<Array<any>>('http://localhost:8080/getAllEmployee');
  }

  /*
   * updateCustomerDetail is used to update the customer detail.
   * @param {any} customer.
   */
  updateCustomerDetail(customer) {
    let newEmployee : { [key:string]: any} = {
        key: customer.key,
        name: customer.name,
        phone: customer.phone,
        mail: customer.mail,
        place: customer.place,
        birthDate: customer.birthDate ? customer.birthDate.toDateString() : '',
        status: customer.status
    };
    return this.http.put('http://localhost:8080/editEmployee', newEmployee);

  }

  /*
   * deleteCustomer is used to delete the customer from the list of customer details.
   * @param {any} customer.
   */
  deleteCustomer(customer) {
    return this.http.delete('http://localhost:8080/deleteEmployee?key='+customer.key);
  }
}
