import { Injectable } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

/*
 * ConfirmDialogService is used for configuring confirmation dialog.
 *
 * @author Mohan Chidambaram
 */
@Injectable({
  providedIn: 'root'
})
export class ConfirmDialogService {

  constructor(private dialog:MatDialog) { }

  /*
   * openConfirmationdialog is used to open the configured confirmation dialog.
   * @param {any} content.
   */
  openConfirmationdialog (content) {
    return this.dialog.open(ConfirmDialogComponent,{
      width: '390px',
      panelClass: 'confirm-dialog-container',
      disableClose: true,
      data:{
        message:content
      }
    });
  }
}
