import { TestBed } from '@angular/core/testing';

import { BiztbaaCustomerService } from './biztbaa-customer.service';

describe('BiztbaaCustomerService', () => {
  let service: BiztbaaCustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BiztbaaCustomerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
