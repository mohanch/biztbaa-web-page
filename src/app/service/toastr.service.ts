import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";

/*
 * ToastrService is used for showing snack bar in the screen.
 *
 * @author Mohan Chidambaram
 */
@Injectable({
  providedIn: 'root'
})
export class ToastrService {

  constructor(public snackBar: MatSnackBar) { }

  config: MatSnackBarConfig = {
    duration: 3000,
    horizontalPosition: 'left',
    verticalPosition: 'bottom'
  }

  /*
   * success is used to show success toastr in the screen.
   * @param {string} msg.
   */
  success(msg) {
    this.config['panelClass'] = ['notification','success'];
    this.snackBar.open(msg, '', this.config);
  }

  /*
   * fail is used to show failiure toastr in the screen.
   * @param {string} msg.
   */
  fail(msg) {
    this.config['panelClass'] = ['notification','warn'];
    this.snackBar.open(msg, '', this.config);
  }
}
